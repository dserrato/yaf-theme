<?php

/**
 * Implements template_preprocess_html().
 *
 */
//function yaf_preprocess_html(&$variables) {
//  // Add conditional CSS for IE. To use uncomment below and add IE css file
//  drupal_add_css(path_to_theme() . '/css/ie.css', array('weight' => CSS_THEME, 'browsers' => array('!IE' => FALSE), 'preprocess' => FALSE));
//
//  // Need legacy support for IE downgrade to Foundation 2 or use JS file below
//  // drupal_add_js('http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js', 'external');
//}

/**
 * Implements template_preprocess_page
 *
 */
//function yaf_preprocess_page(&$variables) {
//}

/**
 * Implements template_preprocess_node
 *
 */
//function yaf_preprocess_node(&$variables) {
//}

function yaf_preprocess_page(&$variables) {
// Convenience variables
  if (!empty($variables['page']['sidebar_first'])){
    $left = $variables['page']['sidebar_first'];
  }

  if (!empty($variables['page']['sidebar_second'])) {
    $right = $variables['page']['sidebar_second'];
  }
// Dynamic sidebars
  if (!empty($left) && !empty($right)) {
    $variables['main_grid'] = 'large-4 large-push-4';
    $variables['sidebar_first_grid'] = 'large-4 large-pull-4';
    $variables['sidebar_sec_grid'] = 'large-4';
  } elseif (empty($left) && !empty($right)) {
    $variables['main_grid'] = 'large-8';
    $variables['sidebar_first_grid'] = '';
    $variables['sidebar_sec_grid'] = 'large-4';
  } elseif (!empty($left) && empty($right)) {
    $variables['main_grid'] = 'large-8 large-push-4';
    $variables['sidebar_first_grid'] = 'large-4 large-pull-8';
    $variables['sidebar_sec_grid'] = '';
  } else {
    $variables['main_grid'] = 'large-12';
    $variables['sidebar_first_grid'] = '';
    $variables['sidebar_sec_grid'] = '';
  }
}




function yaf_field__taxonomy_term_reference($variables) {
	$output = '';

 // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label">' . $variables['label'] . ':&nbsp</div>';
  }

	// Render the items.
  
  foreach ($variables['items'] as $delta => $item) {
    $output .= drupal_render($item);
  }

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';

  return $output;
}



/**
 * Implements hook_preprocess_button().
 */
function yaf_preprocess_button(&$variables) {
//  $variables['element']['#attributes']['class'][] = 'button';
//  if (isset($variables['element']['#parents'][0]) && $variables['element']['#parents'][0] == 'submit') {
    $variables['element']['#attributes']['class'][] = 'tiny';
//  }
}

function yaf_menu_local_task(&$variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];
  $li_class = (!empty($variables['element']['#active']) ? ' class="active"' : '');

  if (!empty($variables['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }

  // Add section tab styling
  $link['localized_options']['attributes']['class'] = array('tiny', 'button', 'secondary');

  $output = '';
  $output .= '<li' . $li_class . '>';
  $output .= l($link_text, $link['href'], $link['localized_options']);
  $output .= "</li>\n";
  return  $output;
}

function yaf_form_alter(&$form, $form_state, $form_id) {
	// Check for a particular content type's node form.
  if ($form_id == 'coordinator_account_request_entityform_edit_form') {
  	foreach($form['field_ef_coord_schools']['und'] as $k=>$v) {
  		if(!is_numeric($k)) //we only want the individual items...
    	continue;
  		unset ($form['field_ef_coord_schools']['und'][$k]['remove_button']);
		}		
	}
}