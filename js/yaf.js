(function ($, Drupal) {

  Drupal.behaviors.yaf = {
    attach: function(context, settings) {
      // Get your Yeti started.
    }
  };

  Drupal.behaviors.editablefields_submit = {
	  attach: function (context) {
	    $('.editablefield-item').once('editablefield', function() {
	      var $this = $(this);

	      // There is only one editable field in that form, we can hide the submit
	      // button.
	      if ($this.find('input[type=text],textarea,select').length == 1) {
	        $this.find('button.form-submit').hide();
	        $this.find('button[type=text],textarea,select').change(function() {
	          $this.find('button.form-submit').triggerHandler('click');
	        });
	      }
	    });
	  }
	}; 

})(jQuery, Drupal);
