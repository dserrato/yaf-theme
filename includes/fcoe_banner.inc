<!-- fcoe banner -->
  <div id="fcoe-banner">
    <div class="row">
      <div class="large-2 medium-12 columns">
        <div class="fcoe-logo"><a href="http://www.fcoe.org"></a></div>
      </div>
      <div class="large-3 medium-3 show-for-large-up columns">
        <h6 class="antialiased">Fresno County Office of Education </h6>
      </div>
      <div class="large-7 medium-6 show-for-large-up columns">
        <h6 class="antialiased">Jim A. Yovino, Superintendent of Schools</h6>
      </div>
    </div>
  </div>